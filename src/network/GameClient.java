package network;

import java.net.*;

public class GameClient implements Runnable
{
	private Socket socket;  //the client connection to the network
	private util.GameAccept game;

	private GameReadSocketClientVisitor game_read;
	private GameWriteSocketClientVisitor game_write;

   public GameClient(util.GameAccept ga, String host_ip, int port_num)
   {
      try
      {
         socket = new Socket(host_ip, port_num);
         System.out.println("Connected!");
      }
      catch(java.io.IOException ioe)
      {
         System.out.println("Unable to connect to server.");
         System.exit(0);
      }

	  game = ga;

	  //DO THIS
		//set up the client read socket class by creating an object on game_read, passing socket

		game_read = new GameReadSocketClientVisitor(socket);

		//accept game_read into game
		
		game.accept(game_read);


		//set up the client write socket class by creating an object on game_write, passing socket and client ID that you can get from game_read

		game_write = new GameWriteSocketClientVisitor(game_read.getClientID(), socket);

		//create a new Thread, passing "this" as a parameter
		
		Thread thread = new Thread();
		thread.start();


	  //start the thread
	  
	  run();


   }

   public void run()
   {
	   //DO THIS
	   //while the game is not over, see whose turn it is (isClientTurn, from game_read)
	   //while this client is taking their turn (another loop, checking is_client_turn)
       //then check periodically (every 2 seconds) to see if they have finished
			 //send the client's move to the server by accepting game_write on game
			 //update is_client_turn (from game_write)
	   //when done, the current player will send the game state to the server

	   //at the bottom of the outer loop, all clients block until the server updates the game state
	   //the client whose turn it is now enters the inner loop and takes their turn by accepting game_read on game

	   while(!game_read.isGameOver()){
			if(game_read.isClientTurn()){
				while(true){
					try{
						Thread.sleep(2000);
					}catch(InterruptedException ie) {}
					
					game.accept(game_write);
					
					if(!game_write.isClientTurn()){					
						break;
					}
				}

			}
			
			game.accept(game_read);
		}


   }
}
