package network;

import java.net.*;

public class GameServer
{
   private Socket[] clients;
   private util.GameAccept game;

   private GameReadSocketServerVisitor game_read;
   private GameWriteSocketServerVisitor game_write;

   public GameServer(util.GameAccept ga, int port_num, int num_clients)
   {
	   game = ga;
      clients = new Socket[num_clients];

      //watch out, this is threaded
	  //blocks for all clients to connect
      startServer(port_num, clients);

	  //DO THIS
	  //set up the server socket classes by creating objects on game_read and game_write, passing clients

	  
	  game_read = new GameReadSocketServerVisitor(clients);
	  game_write = new GameWriteSocketServerVisitor(clients);
	  
	  //create a new Thread passing "this" as a parameter
	  
	  Thread thread = new Thread();

	  
	  //start the thread
	
	thread.start();

   }

   //the server simply collects and disseminates the game state periodically
   public void run()//server loop
   {
     //in this method, you will be using the Visitor Design Pattern
	   //DO THIS
	   //accept game_write on game
	   
	   game.accept(game_write);
	   


     //while the game is not over


        //write to clients by accepting game_write on game


        //receive from active player by accepting game_read on game
		
		while (!game_read.isGameOver()){
		   game.accept(game_write);
		   game.accept(game_read);
	   }


     //at this point, the game is over, so we need to send the final document to all of the clients
	   //accept game_write on game
	   
	   game.accept(game_write);
     

   }

   public void startServer(int port, Socket[] clients)
   {
      int num_clients = clients.length;

      try
      {
         //start the server
         ServerSocket welcomeSocket = new ServerSocket(port);
         int num_connect = 0;

         while(num_connect < num_clients)
         {
            Socket connection_socket = welcomeSocket.accept();
            clients[num_connect] = connection_socket;
            num_connect++;
         }
      }
      catch (java.io.IOException ioe) {}

	  //blocking code
      //wait for all clients before proceeding
      //this is necessary as the above code is threaded
      boolean wait = true;
      while(wait)
      {
			wait = false;

			try
			{
				Thread.sleep(1000);  //1 second
			}
			catch(InterruptedException ie) {}

			for (int i = 1; i <= num_clients; i++)
			{
				if (clients[i - 1] == null)
				{
					wait = true;
					break;
				}
			}
		}
   }
}
